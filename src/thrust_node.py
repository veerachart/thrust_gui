#!/usr/bin/env python

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import rospy
from crazyflie.msg import Command

class gui(QWidget):
    def __init__(self, parent=None):
        super(gui, self).__init__(parent)
        
        rospy.init_node('thruster_gui_node')
        
        self.thruster = QSlider(Qt.Horizontal)
        self.thruster.setRange(0,60000)
        self.thruster.setMinimumWidth(640)
        self.thruster.setSingleStep(100)
        self.thruster.setValue(0)
        
        self.thruster_box = QLineEdit()
        self.thruster_box.setReadOnly(True)
        self.thruster_box.setMaximumWidth(120)
        self.thruster_box.setFrame(False)
        self.thruster_box.setText(str(self.thruster.value()))
        
        self.thruster.valueChanged.connect(self.thrusterChanged)
        
        self.roll_label = QLabel("Roll")
        self.pitch_label = QLabel("Pitch")
        self.yaw_label = QLabel("Yaw")
        
        self.roll_box = QLineEdit()
        self.roll_box.setReadOnly(True)
        self.roll_box.setFrame(False)
        self.roll_box.setText(str(0.0))
        
        self.pitch_box = QLineEdit()
        self.pitch_box.setReadOnly(True)
        self.pitch_box.setFrame(False)
        self.pitch_box.setText(str(0.0))
        
        self.yaw_box = QLineEdit()
        self.yaw_box.setReadOnly(True)
        self.yaw_box.setFrame(False)
        self.yaw_box.setText(str(0.0))
        
        self.cont_angle = QHBoxLayout()
        self.cont_angle.addWidget(self.roll_label)
        self.cont_angle.addWidget(self.roll_box)
        self.cont_angle.addWidget(self.pitch_label)
        self.cont_angle.addWidget(self.pitch_box)
        self.cont_angle.addWidget(self.yaw_label)
        self.cont_angle.addWidget(self.yaw_box)
        
        self.main_layout = QGridLayout()
        self.main_layout.addWidget(self.thruster,0,0)
        self.main_layout.addWidget(self.thruster_box,0,1)
        self.main_layout.addLayout(self.cont_angle,1,0)
        
        self.setLayout(self.main_layout)
        self.setWindowTitle("Crazyflie Thrust Controller")
        
        self.cmd = Command()
        self.cmd.thrust = self.thruster.value()
        
        self.command_pub = rospy.Publisher('command', Command)
        
    def thrusterChanged(self):
        self.cmd.thrust = self.thruster.value()
        self.thruster_box.setText(str(self.thruster.value()))
        self.run_node()
        
    def run_node(self):
        self.command_pub.publish(self.cmd)
        
    def keyPressEvent(self, e):
        if not e.isAutoRepeat():
            if e.key() == Qt.Key_W:
                self.pitchDown()
            elif e.key() == Qt.Key_X:
                self.pitchUp()
            elif e.key() == Qt.Key_A:
                self.rollLeft()
            elif e.key() == Qt.Key_D:
                self.rollRight()
            elif e.key() == Qt.Key_Q:
                self.yawCcw()
            elif e.key() == Qt.Key_E:
                self.yawCw()
            elif e.key() == Qt.Key_S:
                self.reset()
            elif e.key() == Qt.Key_Space:
                self.stop()
            else:
                pass
        else:
            pass
        
    def pitchUp(self):
        self.cmd.pitch = self.cmd.pitch + 0.1       # 0.1 degree increment
        self.pitch_box.setText(str(round(self.cmd.pitch,1)))
        self.run_node()
    def pitchDown(self):
        self.cmd.pitch = self.cmd.pitch - 0.1       # 0.1 degree decrement
        self.pitch_box.setText(str(round(self.cmd.pitch,1)))
        self.run_node()
    def rollRight(self):
        self.cmd.roll = self.cmd.roll + 0.1         # 0.1 degree increment
        self.roll_box.setText(str(round(self.cmd.roll,1)))
        self.run_node()
    def rollLeft(self):
        self.cmd.roll = self.cmd.roll - 0.1         # 0.1 degree decrement
        self.roll_box.setText(str(round(self.cmd.roll,1)))
        self.run_node()
    def yawCw(self):
        self.cmd.yaw = self.cmd.yaw + 0.1           # 0.1 degree increment
        self.yaw_box.setText(str(round(self.cmd.yaw,1)))
        self.run_node()
    def yawCcw(self):
        self.cmd.yaw = self.cmd.yaw - 0.1           # 0.1 degree decrement
        self.yaw_box.setText(str(round(self.cmd.yaw,1)))
        self.run_node()
    def reset(self):                                # Reset all angles to 0
        self.cmd.roll = 0.0
        self.cmd.pitch = 0.0
        self.cmd.yaw = 0.0
        self.roll_box.setText(str(self.cmd.roll))
        self.pitch_box.setText(str(self.cmd.pitch))
        self.yaw_box.setText(str(self.cmd.yaw))
        self.run_node()
    def stop(self):
        rospy.loginfo("Prepare for landing")
        while self.cmd.thrust > 25000:              # Reduce speed
            self.cmd.thrust = self.cmd.thrust - 500
            self.run_node()
            rospy.sleep(0.5)
        self.cmd.thrust = 0         # set everything to zero
        self.thruster.setValue(0)   # change the value on slider back to zero
        self.reset()
        self.run_node()
#class thruster_node:
#    def __init__(self):
#        import sys
#        self.app = QApplication(sys.argv)
#        
#        self.screen = gui()
#        self.screen.show()
#        
#        # ROS variable
#        self.command_pub = rospy.Publisher('command', Command)
#        
#    def run_node(self):
#        self.command_pub.publish(self.screen.cmd)
        
#def run():
#    rospy.init_node('thruster_gui_node')
#    node = thruster_node()
#    while not rospy.is_shutdown():
#        print 'Anything happened?'
#        node.run_node()
#        rospy.sleep(0.1)
#
#if __name__ == '__main__':
#    run()
    
if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    screen = gui()
    screen.show()
    
    sys.exit(app.exec_())
