A GUI for sending thrust command to Crazyflie quadrotor

The slider bar has value of 0-60000. The bar's value is sent as the value for 
thrust by publishing to topic 'command' of type Crazyflie/Command. Roll, pitch,
and yaw commands are set to zero.
